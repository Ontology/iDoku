﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TypedTaggingModule.Models;

namespace WebApplication1.Models
{
    public class Reference
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<TypedTagWithHtmlPage> Tags { get; set; }
    }
}