﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDoku.Models
{
    public class IDokuViewModel
    {
        public string IdRef { get; set; }
        public bool LoadScript { get; set; }
    }
}