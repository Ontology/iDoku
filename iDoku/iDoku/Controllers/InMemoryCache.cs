﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Caching;

namespace IDoku.Controllers
{
    public class InMemoryCache : ICacheService
    {
        private DateTime lastRefresh;
        public T GetOrSet<T>(string cacheKey, Func<T> getItemCallback) where T : class
        {
            T item = null;
            var lastRefreshItem = MemoryCache.Default.Get(nameof(lastRefresh)) as DateTime?;

            if (lastRefreshItem == null)
            {
                lastRefreshItem = DateTime.Now;
                
            }
            
            lastRefresh = lastRefreshItem.Value;


            if (DateTime.Now.Subtract(lastRefresh).TotalHours < 2)
            {
                item = MemoryCache.Default.Get(cacheKey) as T;
            }
            
            if (item == null)
            {
                lastRefresh = DateTime.Now;
                MemoryCache.Default.Add(nameof(lastRefresh), lastRefresh, DateTime.Now.AddMinutes(10));
                item = getItemCallback();
                MemoryCache.Default.Add(cacheKey, item, DateTime.Now.AddMinutes(10));
                
            }
            return item;
        }
    }

    interface ICacheService
    {
        T GetOrSet<T>(string cacheKey, Func<T> getItemCallback) where T : class;
    }
}
