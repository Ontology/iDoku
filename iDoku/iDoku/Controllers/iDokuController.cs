﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using IDoku.Models;
using System.Text.RegularExpressions;
using System.Text;
using TypedTaggingModule.Models;
using WebApplication1.Models;
using OntoWebCore.Models;
using HtmlEditorController.Connectors;

namespace IDoku.Controllers
{
    public class IDokuController : Controller
    {
        private InMemoryCache inMemoryCache;
        private IDokuViewModel IDokuViewModel;
        private List<clsOntologyItem> allowedPages;
        // GET: IDoku
        [Route("IDoku2")]
        public ActionResult Index(string id, string notLoadScripts)
        {
            var loadScript = true;
            if (!string.IsNullOrEmpty(notLoadScripts))
            {
                loadScript = false;
            }


            IDokuViewModel = new IDokuViewModel
            {
                IdRef = id,
                LoadScript = loadScript
            };

            return View(IDokuViewModel);
        }

        [Route("IDoku")]
        public ActionResult GetHtml(string id)
        {
            inMemoryCache = new InMemoryCache();

            var htmlPath = "/iDoku/iDoku/ToBeContinued";
            var anonymousPath = @"C:\UserGroupRessources\74f38566eda64c76b862e839b6a8d07d\anonymoushtmldocuments.json";
            if (System.IO.File.Exists(anonymousPath))
            {
                var anonymous = inMemoryCache.GetOrSet<ResultAnonymous>("Anonymous", GetAnonymous);


                if (anonymous.AnonymousItems.Any(allowedItem => allowedItem.GUID == id))
                {
                    htmlPath = "https://www.omodules.de/OntologyModules/Resources/UserGroupRessources/74f38566eda64c76b862e839b6a8d07d/" + id + ".html";
                }
                else
                {
                    htmlPath = "/iDoku/iDoku/ToBeContinued";
                }

                if (!System.IO.File.Exists(@"C:\UserGroupRessources\74f38566eda64c76b862e839b6a8d07d\" + id + ".html"))
                {
                    htmlPath = "/iDoku/iDoku/ToBeContinued";
                }
            }
            //var oAItemHtmlDocument = GetOAttHtml(id);

            //return Json(oAItemHtmlDocument, JsonRequestBehavior.AllowGet);
            var result = new { ID_Object = id, Name_Object = id, Url = htmlPath };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Route("IDoku")]
        public ActionResult GetRawHtml(string id)
        {
            inMemoryCache = new InMemoryCache();

            var anonymous = inMemoryCache.GetOrSet<ResultAnonymous>("Anonymous", GetAnonymous);
            var htmlPath = "";
            if (anonymous.AnonymousItems.Any(allowedItem => allowedItem.GUID == id))
            {
                htmlPath = "https://www.omodules.de/OntologyModules/Resources/UserGroupRessources/74f38566eda64c76b862e839b6a8d07d/" + id + ".html";
            }
            else
            {
                htmlPath = "/iDoku/iDoku/ToBeContinued";
            }

            if (!System.IO.File.Exists(@"C:\UserGroupRessources\74f38566eda64c76b862e839b6a8d07d\" + id + ".html"))
            {
                htmlPath = "/iDoku/iDoku/ToBeContinued";
            }
            //var oAItemHtmlDocument = GetOAttHtml(id);
            //var result = new { refId = id, html = oAItemHtmlDocument?.Val_String ?? "" };
            var result = new { refId = id, html = htmlPath };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Route("IDoku")]
        private clsObjectAtt GetOAttHtml(string id)
        {
            inMemoryCache = new InMemoryCache();

            var connector = new HtmlEditorConnector();

            if (!connector.AllowedItems.Any(allowedItem => allowedItem.GUID == id)) return null;

            clsObjectAtt oAItemHtmlDocument = null;
            var connectorResult = connector.GetHtmlDocument(id);
            connectorResult.Wait();
            if (connectorResult.Result.Result.GUID == connector.LocalConfig.Globals.LState_Success.GUID)
            {
                oAItemHtmlDocument = connectorResult.Result.OAItemHtmlDocument;
            }

            var regexChange = new Regex("href=\"HtmlEditorJQ.html.Class=" + connector.LocalConfig.Globals.RegexGuid + "(.|.{5})Object=" + connector.LocalConfig.Globals.RegexGuid + "\"");
            var regexObject = new Regex("(O|o)bject=" + connector.LocalConfig.Globals.RegexGuid);
            var regexObjectId = new Regex(connector.LocalConfig.Globals.RegexGuid);
            var contentBuilder = new StringBuilder();
            if (oAItemHtmlDocument != null && oAItemHtmlDocument.Val_String != null)
            {
                var objectId = "";
                var text = oAItemHtmlDocument.Val_String;
                var matches = regexChange.Matches(text);
                if (matches.Count > 0)
                {
                    var start = 0;
                    matches.Cast<Match>().ToList().ForEach(matchItem =>
                    {
                        var index = matchItem.Index;
                        var length = matchItem.Length;
                        matchItem = regexObject.Match(matchItem.Value);
                        if (matchItem.Success)
                        {
                            var objectString = matchItem.Value;
                            matchItem = regexObjectId.Match(objectString);
                            if (matchItem.Success)
                            {
                                objectId = matchItem.Value;
                            }

                            contentBuilder.Append(oAItemHtmlDocument.Val_String.Substring(start, index - start));
                            contentBuilder.Append("href=\"/iDoku/iDoku/Index/" + objectId + "\"");
                            start = index + length;

                        }

                    });
                    if (start < text.Length)
                    {
                        contentBuilder.Append(oAItemHtmlDocument.Val_String.Substring(start));
                    }
                }
                else
                {
                    contentBuilder.Append(oAItemHtmlDocument.Val_String);
                }
                contentBuilder.Replace("../Resources/UserGroupRessources", "/iDoku/Resources/UserGroupRessources");
                oAItemHtmlDocument.Val_String = contentBuilder.ToString();

            }

            return oAItemHtmlDocument;
        }

        [Route("IDoku")]
        public ActionResult GetTags(string id)
        {
            //inMemoryCache = new InMemoryCache();

            //var connector = inMemoryCache.GetOrSet<HtmlEditorConnector>("HtmlEditorConnector", GetHtmlEditorConnector);
            var connector = new HtmlEditorConnector();
            var tagsTask = connector.GetTags(id);
            tagsTask.Wait();

            var tags = tagsTask.Result.TypedTags.Where(typedTag => typedTag.IdTagParent != "902b2cd20fe64f4992fd97bcf28e2dad" 
                && typedTag.IdTagParent != "f62cf1ccee51475f9c007094f1809b56"
                && typedTag.IdTagParent != "d84fa125dbce44b091539abeb66ad27f"
                && typedTag.IdTagParent != "d0f582137db44882bd15962163015d13").OrderBy(typedTag => typedTag.OrderId).ThenBy(typedTag => typedTag.NameTag);
            var otherRefTypes = tags.Where(typedTag => typedTag.IdTagParent != "ce29760f023043daa19d91c6d82a8437" && typedTag.IdTagParent != "3bb4b05d8fcb48b7a1ac0fb42a265c38").GroupBy(typedTag => new { Id = typedTag.IdTagParent, Name = typedTag.NameTagParent }).OrderBy(refGroup => refGroup.Key.Name).ToList();

            var htmlDocs = connector.GetHtmlDocumentsByRefs(tags.Select(tag => new clsOntologyItem { GUID = tag.IdTag }).ToList());
            htmlDocs.Wait();
            var htmlDocsAllowed = (from htmlDocRel in htmlDocs.Result.HtmlDocumentsToRef
                                   join allowedDoc in connector.AllowedItems on htmlDocRel.ID_Object equals allowedDoc.GUID
                                   select htmlDocRel);

            var tagsWithReferences = (from tag in tags
                                      join htmlDoc in htmlDocsAllowed on tag.IdTag equals htmlDoc.ID_Other into htmlDocs1
                                      from htmlDoc in htmlDocs1.DefaultIfEmpty()
                                      select new TypedTagWithHtmlPage(tag, htmlDoc != null ? "/iDoku/iDoku/Index/" + htmlDoc.ID_Object : ""));

            var references = new List<Reference>();
            var sceneTags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == "ce29760f023043daa19d91c6d82a8437");
            
            if (sceneTags.Any())
            {
                references.Add(new Reference
                {
                    Id = "ce29760f023043daa19d91c6d82a8437",
                    Name = "Szenen",
                    Tags = sceneTags.ToList()
                });
            }

            var termTags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == "3bb4b05d8fcb48b7a1ac0fb42a265c38");

            if (termTags.Any())
            {
                references.Add(new Reference
                {
                    Id = "3bb4b05d8fcb48b7a1ac0fb42a265c38",
                    Name = "Schlagwörter",
                    Tags = termTags.ToList()
                });
            }

            otherRefTypes.ForEach(refGroup =>
            {
                var reference = new Reference
                {
                    Id = refGroup.Key.Id,
                    Name = refGroup.Key.Name
                };

                reference.Tags = tagsWithReferences.Where(typedTag => typedTag.IdTagParent == refGroup.Key.Id).ToList();
                references.Add(reference);
            });

            
            return Json(references, JsonRequestBehavior.AllowGet);
        }

        [Route("IDoku")]
        public ActionResult Diskurstrueb()
        {
            var model = new LiteraturViewModel();
            model.IdLiteratur = "74406dfdce4d47268a7dde83ad606efa";
            
            return View(model);
        }
        
        [Route("IDoku")]
        public ActionResult Erkenntnistheoretisch()
        {
            var model = new LiteraturViewModel();
            model.IdLiteratur = "b56b40cdc8d647bc98c90b2bf62fda62";

            return View(model);
        }
        [Route("IDoku")]
        public ActionResult Politisch()
        {
            var model = new LiteraturViewModel();
            model.IdLiteratur = "cbdd7e0b691e4b668681882bff27633e";

            return View(model);
        }

        [Route("IDoku")]
        public ActionResult Ontologisch()
        {
            var model = new LiteraturViewModel();
            model.IdLiteratur = "ccd1bf3786dc4ebead34454e2a31f64a";

            return View(model);
        }

        [Route("IDoku")]
        public ActionResult GetTreeConfig(string id)
        {
            inMemoryCache = new InMemoryCache();
            var anonymous = inMemoryCache.GetOrSet<ResultAnonymous>("Anonymous", GetAnonymous);
            
            if (!anonymous.AnonymousItems.Any(anonym => anonym.GUID == id))
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

            var treeViewConfig = new KendoTreeViewConfig
            {
                dataSource = new KendoHierarchicalDataSource
                {
                    transport = new KendoTransport
                    {
                        read = new KendoTransportRead
                        {
                            dataType = "JSON",
                            url = "https://www.omodules.de/OntologyModules/Resources/UserGroupRessources/74f38566eda64c76b862e839b6a8d07d/" + id + "_tree.json"
                        }
                    },
                    schema = new KendoHierarchicalSchema
                    {
                        model = new KendoHierarchicalModel
                        {
                            id = nameof(KendoTreeNode.NodeId),
                            children = nameof(KendoTreeNode.SubNodes),
                            hasChildren = nameof(KendoTreeNode.HasChildren)
                        }
                    }
                }

            };

            return Json(treeViewConfig, JsonRequestBehavior.AllowGet);
        }

        

        public ActionResult ToBeContinued()
        {
            return View();
        }

        private ResultAnonymous GetAnonymous()
        {
            var anonymousPath = @"C:\UserGroupRessources\74f38566eda64c76b862e839b6a8d07d\anonymoushtmldocuments.json";
            var content = System.IO.File.ReadAllText(anonymousPath);
            var result = new ResultAnonymous
            {
                AnonymousItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<clsOntologyItem>>(content)
            };
            return result;
        }
    }

    public class ResultAnonymous
    {
        public clsOntologyItem Result { get; set; }
        public List<clsOntologyItem> AnonymousItems { get; set; }
    }
}