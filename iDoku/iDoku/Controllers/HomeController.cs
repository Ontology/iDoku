﻿using HtmlEditorController.Connectors;
using IDoku.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IDoku.Controllers
{
    public class HomeController : Controller
    {
        private InMemoryCache inMemoryCache;
        private IDokuViewModel IDokuViewModel;
        private List<clsOntologyItem> allowedPages;

        public ActionResult Index(string id)
        {
            IDokuViewModel = new IDokuViewModel
            {
                IdRef = id
            };

            return View(IDokuViewModel);
        }

        [Route("IDoku")]
        public ActionResult GetHtml(string id)
        {
            inMemoryCache = new InMemoryCache();

            var connector = inMemoryCache.GetOrSet<HtmlEditorConnector>("HtmlEditorConnector", GetHtmlEditorConnector);

            if (!connector.AllowedItems.Any(allowedItem => allowedItem.GUID == id)) return null;

            clsObjectAtt oAItemHtmlDocument = null;
            var connectorResult = connector.GetHtmlDocument(id);
            connectorResult.Wait();
            if (connectorResult.Result.Result.GUID == connector.LocalConfig.Globals.LState_Success.GUID)
            {
                oAItemHtmlDocument = connectorResult.Result.OAItemHtmlDocument;
            }

            var regexChange = new Regex("href=\"HtmlEditorJQ.html?.*\"");
            var regexObject = new Regex("(O|o)bject=" + connector.LocalConfig.Globals.RegexGuid);
            var regexObjectId = new Regex(connector.LocalConfig.Globals.RegexGuid);
            var contentBuilder = new StringBuilder();
            if (oAItemHtmlDocument != null && oAItemHtmlDocument.Val_String != null)
            {
                var objectId = "";
                var text = oAItemHtmlDocument.Val_String;
                var matches = regexChange.Matches(text);
                if (matches.Count > 0)
                {
                    var start = 0;
                    matches.Cast<Match>().ToList().ForEach(matchItem =>
                    {
                        var index = matchItem.Index;
                        var length = matchItem.Length;
                        matchItem = regexObject.Match(matchItem.Value);
                        if (matchItem.Success)
                        {
                            var objectString = matchItem.Value;
                            matchItem = regexObjectId.Match(objectString);
                            if (matchItem.Success)
                            {
                                objectId = matchItem.Value;
                            }

                            contentBuilder.Append(oAItemHtmlDocument.Val_String.Substring(start, index - start));
                            contentBuilder.Append("href=\"/iDoku/Index/" + objectId + "\"");
                            start = index + length;

                        }

                    });

                }
                else
                {
                    contentBuilder.Append(oAItemHtmlDocument.Val_String);
                }
                oAItemHtmlDocument.Val_String = contentBuilder.ToString();
            }


            return Json(oAItemHtmlDocument, JsonRequestBehavior.AllowGet);
        }

        private HtmlEditorConnector GetHtmlEditorConnector()
        {


            var globals = new Globals(Server.MapPath("~/App_Data/Config/Config_ont.xml"));
            var htmlEditorConnector = new HtmlEditorConnector(globals);
            return htmlEditorConnector;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}