﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IDoku.Startup))]
namespace IDoku
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
